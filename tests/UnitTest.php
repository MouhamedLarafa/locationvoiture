<?php
namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testDemo()
    {

        $client = new Client();

        $client->setCin("testcin");

        $this->assertTrue( $client->getCin()=="testcin");
    }
}